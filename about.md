---
layout: page
title: About
permalink: /about/
---

<a href="https://stackoverflow.com/users/3803219/craigdfrench">
<img src="https://stackoverflow.com/users/flair/3803219.png" width="208" height="58" alt="profile for craigdfrench at Stack Overflow, Q&amp;A for professional and enthusiast programmers" title="profile for craigdfrench at Stack Overflow, Q&amp;A for professional and enthusiast programmers">
</a>  

<a href="https://serverfault.com/users/237560/craigdfrench">
<img src="https://serverfault.com/users/flair/237560.png" width="208" height="58" alt="profile for craigdfrench at Server Fault, Q&amp;A for system and network administrators" title="profile for craigdfrench at Server Fault, Q&amp;A for system and network administrators">
</a>
<script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js"></script>

<div class="LI-profile-badge"  data-version="v1" data-size="large" data-locale="en_US" data-type="vertical" data-theme="dark" data-vanity="craigdfrench"><a class="LI-simple-link" href='https://ca.linkedin.com/in/craigdfrench?trk=profile-badge'>Craig D. French</a></div>
<a title="Keybase" target="_blank" href="https://keybase.io/craigdfrench">
  <img src="https://badgen.net/keybase/pgp/craigdfrench">
</a>

IEEE Membership: [Membership Certificate](https://craigdfrench.keybase.pub/IEEEMemberCertificate.pdf)

This is the base Jekyll theme. You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](http://jekyllrb.com/)

You can find the source code for the Jekyll new theme at:
{% include icon-github.html username="jglovier" %} /
[jekyll-new](https://github.com/jglovier/jekyll-new)


You can find the source code for Jekyll at
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)
